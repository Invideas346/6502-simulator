cmake_minimum_required(VERSION 3.16.3)

project(6502-Simulator)

add_subdirectory(src)