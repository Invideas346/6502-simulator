#pragma once

#include <memory.hpp>
#include <instructions.hpp>

template <typename T>
class Register
{
public:
  Register(T val);
  ~Register();

  T read() const;
  void write(T val);

private:
  T _val;
};

class CPU
{
public:
  CPU();
  ~CPU();

  void exec(Memory& mem);
  void dump();

  const Register<uint16_t>& pc() const;
  const Register<uint8_t>& acc() const;
  const Register<uint8_t>& x() const;
  const Register<uint8_t>& y() const;
  const Register<uint8_t>& sr() const;
  const Register<uint8_t>& sp() const;

private:
  Register<uint16_t> _pc;
  Register<uint8_t> _acc;
  Register<uint8_t> _x;
  Register<uint8_t> _y;
  Register<uint8_t> _sr;
  Register<uint8_t> _sp;

  uint64_t _cyclesCnt = 0;
};
