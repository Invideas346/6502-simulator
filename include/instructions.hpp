#pragma once

#include <cstdint>
#include <vector>
#include <optional>
#include <functional>

enum class OPCODES : uint8_t
{
  ADC_XIN = 0x61, // add with carry
  ADC_INY = 0x71, // add with carry
  ADC_ZPG = 0x65, // add with carry
  ADC_ZPX = 0x75, // add with carry
  ADC_IMM = 0x69, // add with carry
  ADC_ABY = 0x59, // add with carry
  ADC_ABS = 0x6D, // add with carry
  ADC_ABX = 0x7D, // add with carry

  AND_XIN = 0x21, // and (with accumulator)
  AND_INY = 0x31, // and (with accumulator)
  AND_ZPG = 0x25, // and (with accumulator)
  AND_ZPX = 0x35, // and (with accumulator)
  AND_IMM = 0x29, // and (with accumulator)
  AND_ABY = 0x39, // and (with accumulator)
  AND_ABS = 0x2D, // and (with accumulator)
  AND_ABX = 0x3D, // and (with accumulator)

  ASL_ZPG = 0x06, // arithmetic shift left
  ASL_ZPX = 0x16, // arithmetic shift left
  ASL_ACC = 0x0A, // arithmetic shift left
  ASL_ABS = 0x0E, // arithmetic shift left
  ASL_ABX = 0x1E, // arithmetic shift left

  BCC = 0x90, // branch on carry clear
  BCS = 0xB0, // branch in carry set
  BEQ = 0xF0, // Branch on equal

  BIT_ZPG = 0x24, // bit test
  BIT_ABS = 0x2C, // bit test

  BMI = 0x30, // branch on minus (negative set)
  BNE = 0xD0, // branch on not equal (zero clear)
  BPL = 0x10, // branch on plus (negative clear)
  BRK = 0x00, // break / interrupt
  BVC = 0x50, // branch on overflow clear
  BVS = 0x70, // branch on overflow set
  CLC = 0x18, // clear carry
  CLD = 0xD8, // clvector
  CLI = 0x58, // clear interrupt disable
  CLV = 0xB8, // clear overflow

  CMP_XIN = 0xC1, // compare (with accumulator)
  CMP_INY = 0xD1, // compare (with accumulator)
  CMP_ZPG = 0xC5, // compare (with accumulator)
  CMP_ZPX = 0xD5, // compare (with accumulator)
  CMP_IMM = 0xC9, // compare (with accumulator)
  CMP_ABY = 0xD9, // compare (with accumulator)
  CMP_ABS = 0xCD, // compare (with accumulator)
  CMP_ABX = 0xDD, // compare (with accumulator)

  CPX_IMM = 0xE0, // compare with X
  CPX_ZPG = 0xE4, // compare with X
  CPX_ABS = 0xEC, // compare with X

  CPY_IMM = 0xC0, // compare with Y
  CPY_ZPG = 0xC4, // compare with Y
  CPY_ABS = 0xDC, // compare with Y

  DEC_ZPG = 0xC6, // decrement accumulator
  DEC_ZPX = 0xD6, // decrement accumulator
  DEC_ABS = 0xCE, // decrement accumulator
  DEC_ABX = 0xDE, // decrement accumulator

  DEX = 0xCA, // decrement X
  DEY = 0x88, // decrement Y

  EOR_XIN = 0x41, // exclusive or (with accumulator)
  EOR_INY = 0x51, // exclusive or (with accumulator)
  EOR_ZPG = 0x45, // exclusive or (with accumulator)
  EOR_ZGX = 0x55, // exclusive or (with accumulator)
  EOR_IMM = 0x49, // exclusive or (with accumulator)
  EOR_ABY = 0x59, // exclusive or (with accumulator)
  EOR_ABS = 0x4D, // exclusive or (with accumulator)
  EOR_ABX = 0x5D, // exclusive or (with accumulator)

  INC_ZPG = 0xE6, // increment accumulator
  INC_ZPX = 0xF6, // increment accumulator
  INC_ABS = 0xEE, // increment accumulator
  INC_ABX = 0xFE, // increment accumulator

  INX = 0xE8, // increment X
  INY = 0xC8, // increment Y

  JMP_ABS = 0x4C, // jump
  JMP_IND = 0x5C, // jump

  JSR = 0x20, // jump subroutine

  LDA_XIN = 0xA1, // load accumulator
  LDA_INY = 0xB1, // load accumulator
  LDA_ZPG = 0xA5, // load accumulator
  LDA_ZPX = 0xB5, // load accumulator
  LDA_IMM = 0xA9, // load accumulator
  LDA_ABY = 0xB9, // load accumulator
  LDA_ABS = 0xAD, // load accumulator
  LDA_ABX = 0xBD, // load accumulator

  LDX_IMM = 0xA2, // load X
  LDX_ZPG = 0xA6, // load X
  LDX_ZPY = 0xC6, // load X
  LDX_ABS = 0xAE, // load X
  LDX_ABY = 0xBE, // load X

  LDY_ZPX = 0xB4, // load Y
  LDY_ABS = 0xAC, // load Y
  LDY_ABX = 0xBC, // load Y

  LSR_ZPG = 0x46, // logical shift right
  LSR_ZPX = 0x56, // logical shift right
  LSR_ACC = 0x4A, // logical shift right
  LSR_ABS = 0x4E, // logical shift right
  LSR_ABX = 0x5E, // logical shift right

  NOP = 0xEA, // no operation

  ORA_XIN = 0x01, // or with accumulator
  ORA_INY = 0x11, // or with accumulator
  ORA_ZPG = 0x05, // or with accumulator
  ORA_ZPX = 0x15, // or with accumulator
  ORA_IMM = 0x09, // or with accumulator
  ORA_ABY = 0x19, // or with accumulator
  ORA_ABS = 0x0D, // or with accumulator
  ORA_ABX = 0x1D, // or with accumulator

  PHA = 0x48, // push accumulator
  PHP = 0x08, // push processor status (SR)

  PLA = 0x68, // pull accumulator
  PLP = 0x28, // pull processor status (SR)

  ROL_ZPG = 0x26, // rotate left
  ROL_ZPX = 0x36, // rotate left
  ROL_ACC = 0x2A, // rotate left
  ROL_ABS = 0x2E, // rotate left
  ROL_ABX = 0x3E, // rotate left

  ROR_ZPG = 0x66, // rotate right
  ROR_ZPX = 0x76, // rotate right
  ROR_ACC = 0x6A, // rotate right
  ROR_ABS = 0x6E, // rotate right
  ROR_ABX = 0x7E, // rotate right

  RTI = 0x40, // return from interrupt
  RTS = 0x60, // return from subroutine

  SBC_XIN = 0xE1, // subtract with carry
  SBC_INY = 0xF1, // subtract with carry
  SBC_ZPG = 0xE5, // subtract with carry
  SBC_ZPX = 0xF5, // subtract with carry
  SBC_ACC = 0xE9, // subtract with carry
  SBC_ABY = 0xF9, // subtract with carry
  SBC_ABS = 0xED, // subtract with carry
  SBC_ABX = 0xFD, // subtract with carry

  SEC = 0x38, // set carry
  SED = 0xF8, // set decimal
  SEI = 0x78, // set interrupt disable

  STA_XIN = 0x81, // store accumulator
  STA_INY = 0x91, // store accumulator
  STA_ZPG = 0x85, // store accumulator

  STX_ZPG = 0x86, // store x
  STX_ZPY = 0x96, // store x
  STX_ABX = 0x8E, // store x

  STY_ZPG = 0x84, // store y
  STY_ZPX = 0x94, // store y
  STY_ABS = 0x8C, // store y

  TAX = 0xAA, // transfer accumulator to X
  TAY = 0xA8, // transfer accumulator to Y
  TSX = 0xBA, // transfer stack pointer to X
  TXA = 0x8A, // transfer X to accumulator
  TXS = 0x9A, // transfer X to stack pointer
  TYA = 0x98, // transfer Y to accumulator
};

enum class AddressMode : int
{
  Accumulator = 0,
  Absolute, AbsoluteX, AbsoluteY,
  Immediate, Implied, Indirect,
  XIndexed, YIndexed,
  Relative,
  ZeroPage, ZeroPageX, ZeroPageY
};

class CPU;
class Memory;
uint8_t getAddrModVal(CPU& cpu, Memory& mem, AddressMode mode, uint16_t opr = 0xFF);

using InstructionOper = void(CPU&, Memory&, std::optional<uint8_t>);

class Instruction
{
public:
  Instruction();
  Instruction(OPCODES code, const std::vector<uint8_t>& args);

  ~Instruction();

  static Instruction make();
  static Instruction make(OPCODES code, const std::vector<uint8_t>& args);

private:
  OPCODES _code;
  std::vector<uint8_t> _args;
  std::function<InstructionOper> _oper;
};
