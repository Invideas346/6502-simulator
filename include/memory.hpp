#pragma once

#include <array>
#include <instructions.hpp>

typedef uint16_t Address;

class Memory
{
public:
  static constexpr Address STACK_START = 0x0100;
  static constexpr Address STACK_END = 0x01FF;

  static constexpr Address ZERO_PAGE_START = 0x0000;
  static constexpr Address ZERP_PAGE_END = 0x00FF;

  static constexpr Address NMI_START = 0xFFFA;
  static constexpr Address NMI_END = 0xFFFB;

  static constexpr Address RES_START = 0xFFFC;
  static constexpr Address RES_END = 0xFFFD;

  static constexpr Address IRQ_START = 0xFFFE;
  static constexpr Address IRQ_END = 0xFFFF;
  static constexpr Address PRG_START = 0x4000;

public:
  explicit Memory(uint8_t defaultVal = 0x00);
  virtual ~Memory();

  uint8_t read_8(Address addr) const;
  uint16_t read_16(Address addr) const;

  void write(Address addr, uint8_t val);
  void write(Address addr, uint16_t val);

private:
  std::array<uint8_t, 0xFFFF> _mem;
};
