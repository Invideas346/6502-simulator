#include <cpu.hpp>

#include <iostream>

CPU::CPU()
  : _x{0}
    , _y{0}
    , _acc{0}
    , _sr{0}
    , _sp{0x00}
    , _pc{Memory::PRG_START} {
}

CPU::~CPU() {

}

void CPU::exec(Memory& mem) {
  // Fetch Instruction

  // Decode Ins.

  // Execute Ins.

  // Repeat
}

void CPU::dump() {
  std::cout << "Cycles elapsed:" << _cyclesCnt << "\n\n";

  std::cout << "ACC:" << _acc.read() << "\n";
  std::cout << "X:" << _x.read() << "\n";
  std::cout << "Y:" << _y.read() << "\n";
  std::cout << "PC:" << _pc.read() << "\n";
  std::cout << "SP:" << _sp.read() << "\n";
  std::cout << "SR:" << _sr.read() << "\n";
}

const Register<uint16_t>& CPU::pc() const { return _pc; }
const Register<uint8_t>&  CPU::acc() const { return _acc; }
const Register<uint8_t>&  CPU::x() const { return _x; }
const Register<uint8_t>&  CPU::y() const { return _y; }
const Register<uint8_t>&  CPU::sr() const { return _sr; }
const Register<uint8_t>&  CPU::sp() const { return _sp; }

template <typename T>
Register<T>::Register(T val): _val{val} {
}

template <typename T>
Register<T>::~Register() {
}

template <typename T>
T Register<T>::read() const {
  return _val;
}

template <typename T>
void Register<T>::write(T val) {
  _val = val;
}