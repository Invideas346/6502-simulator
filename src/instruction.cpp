#include <instructions.hpp>

#include <cpu.hpp>
#include <iostream>

#include <assert.h>

static inline std::function<InstructionOper> getOper(OPCODES code) {
  switch (code) {
    default: {
      std::cerr << "undefined opcode passed in" << std::endl;
      std::exit(EXIT_FAILURE);
    }
  }
}

Instruction::Instruction()
  : _code{OPCODES::NOP}
    , _args{} {
}

Instruction::Instruction(OPCODES code, const std::vector<uint8_t>& args)
  : _code{code}
    , _args{args} {
}

Instruction::~Instruction() {
}

Instruction Instruction::make() {
  return Instruction({
      OPCODES::NOP,
      {}
  });
}

Instruction Instruction::make(OPCODES code, const std::vector<uint8_t>& args) {
  return Instruction({
      code,
      args
  });
}

uint8_t getAddrModVal(CPU& cpu, Memory& mem, AddressMode mode, uint16_t opr) {
  switch (mode) {
    case AddressMode::Accumulator: return cpu.acc().read();
    case AddressMode::Absolute: return mem.read_8(opr);
    case AddressMode::AbsoluteX: return mem.read_8(opr + cpu.x().read());
    case AddressMode::AbsoluteY: return mem.read_8(opr + cpu.y().read());
    case AddressMode::Immediate: return opr & 0xFF;
    case AddressMode::Implied: return opr;
    case AddressMode::Indirect: return mem.read_8(mem.read_16(opr));
    case AddressMode::XIndexed: return mem.read_8(
          mem.read_16(Memory::ZERO_PAGE_START + opr + cpu.x().read()));
    case AddressMode::YIndexed: return mem.read_8(
          mem.read_16(Memory::ZERO_PAGE_START + opr) + cpu.y().read());
    case AddressMode::Relative: return mem.read_8(cpu.pc().read() + (int8_t)opr);
    case AddressMode::ZeroPage: return mem.read_8(Memory::ZERO_PAGE_START + opr);
    case AddressMode::ZeroPageX: return mem.read_8(Memory::ZERO_PAGE_START + cpu.x().read());
    case AddressMode::ZeroPageY: return mem.read_8(Memory::ZERO_PAGE_START + cpu.y().read());
    default: {
      std::cerr << "unknown address mode used" << (int)mode << std::endl;
      return 0;
    }
  }
}