#include <iostream>

#include <simulator.hpp>

int main(int argc, char const* argv[]) {
  Memory mem{0};
  CPU    cpu;
  cpu.exec(mem);
  return 0;
}