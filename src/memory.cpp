#include <memory.hpp>

Memory::Memory(uint8_t defaultVal)
  : _mem{0} {

}

Memory::~Memory() {

}

uint8_t Memory::read_8(Address addr) const {
  return _mem[addr];
}

uint16_t Memory::read_16(Address addr) const {
  return (_mem[addr] | (_mem[addr + 1] << 8));
}

void Memory::write(Address addr, uint8_t val) {
  _mem[addr] = val;
}

void Memory::write(Address addr, uint16_t val) {
  _mem[addr]     = val & 0xFF;
  _mem[addr + 1] = (val & 0xFF << 8) >> 8;
}